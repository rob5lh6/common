cmake_minimum_required(VERSION 2.8.3)
project(XMLOperations)


find_package(catkin REQUIRED COMPONENTS
  roscpp
  roslib
  LoggerSys
)

# find and setup Qt4 for this project
set(QT_QMAKE_EXECUTABLE /usr/bin/qmake-qt4) #Force the system to use QT4
find_package(Qt4 REQUIRED)

SET(QT_USE_QTXML true)
SET(QT_USE_QTNETWORK true)
SET(QT_USE_QTGUI true)
SET(QT_USE_QTCORE true)
SET(QT_USE_QDBUS true)

include(${QT_USE_FILE})

INCLUDE_DIRECTORIES(
   ${QT_INCLUDE_DIR}
)

include_directories(${CMAKE_CURRENT_BINARY_DIR})
include_directories(${QT_INCLUDE_DIR})

#set(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)
#set(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/lib)

#uncomment if you have defined messages
#rosbuild_genmsg()
#uncomment if you have defined services
#rosbuild_gensrv()

set(qt_srcs
XMLOperations.cpp
)

set(qt_hdrs
include/XMLOperations.hpp
)

qt4_automoc(${qt_srcs})

QT4_WRAP_CPP(qt_moc_srcs ${qt_hdrs})


###################################
## catkin specific configuration ##
###################################
catkin_package(
  INCLUDE_DIRS include
  LIBRARIES XMLOperations
  CATKIN_DEPENDS
  roscpp
  roslib
  LoggerSys
  DEPENDS Boost
)

###########
## Build ##
###########

include_directories(
  ${catkin_INCLUDE_DIRS}
  ${Boost_INCLUDE_DIRS}
  include
)


add_library(XMLOperations XMLOperations.cpp ${qt_srcs} ${qt_moc_srcs})

target_link_libraries(XMLOperations
                      ${catkin_LIBRARIES}
                      ${QT_LIBRARIES}
                      ${Boost_LIBRARIES}
                      )
                      
install(TARGETS XMLOperations
        ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
        LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
        RUNTIME DESTINATION ${CATKIN_GLOBAL_BIN_DESTINATION})
                      
                      
install(DIRECTORY include/
        DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
        FILES_MATCHING PATTERN "*.h")
